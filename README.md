# SAP Cloud SDK TypeScript version demo

## Build App

In `app` folder:

* start `npm run watch:local`
* build `npm run ci-build`

## Publish Application

In root folder:

`cf push`

## Next steps

* Add grunt
* Add CI/CD on GitLab
* Add docker compose for test locally
