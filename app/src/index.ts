import app from "./application";
const cfenv = require("cfenv");

const appEnv = cfenv.getAppEnv();

const port = 8080;

app.listen(appEnv.port || port, appEnv.bind, () => {
  console.log("Express server listening on port " + appEnv.url);
});

export default app;