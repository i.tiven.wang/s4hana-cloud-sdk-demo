import express from "express";
import { helloWorld } from "./hello-world-route";
import { indexRoute } from "./index-route";
import { businessPartners } from './business-partner-route';
import { operationalAcctgDocItemCube } from './accounting-document-route';
const passport = require('passport');
const xsenv = require('@sap/xsenv');
const JWTStrategy = require('@sap/xssec').JWTStrategy;
const cfenv = require("cfenv");

const appEnv = cfenv.getAppEnv();

class App {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }

  private config(): void {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));

    if(!appEnv.isLocal) {
      const services = xsenv.getServices({ uaa:'my-xsuaa' });
      passport.use(new JWTStrategy(services.uaa));

      this.app.use(passport.initialize());
      this.app.use(passport.authenticate('JWT', { session: false }));
    }
    
  }

  private routes(): void {
    const router = express.Router();

    router.get("/", indexRoute);
    router.get("/hello", helloWorld);
    router.get('/business-partners', businessPartners);
    router.get('/accounting', operationalAcctgDocItemCube);
    this.app.use("/", router);
  }

}

export default new App().app;