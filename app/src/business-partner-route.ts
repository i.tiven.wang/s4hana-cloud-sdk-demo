import { Request, Response } from 'express';
import { BusinessPartner, Customer } from '@sap/cloud-sdk-vdm-business-partner-service';
import { or, and } from '@sap/cloud-sdk-core';

function getAllBusinessPartners(): Promise<BusinessPartner[]> {
    return BusinessPartner.requestBuilder()
        .getAll()
        .select(
            BusinessPartner.FIRST_NAME,
            BusinessPartner.LAST_NAME,
            BusinessPartner.BUSINESS_PARTNER_FULL_NAME,
            BusinessPartner.TO_CUSTOMER.select(
              Customer.CUSTOMER
            )
          )
        //   .filter(or(
        //       and(
        //         BusinessPartner.FIRST_NAME.equals('Jason'),
        //         BusinessPartner.LAST_NAME.equals('Hoskins'),
        //       )
        //   ))
          .top(10)
        //   .skip(0)
        // .withCustomHeaders({
        //     'APIKey': '9OLeNbRsXlZ3U2faS6bD1TnsBCYaU6gn'
        // })
        .execute({
            destinationName: 'Sandbox'
        });
}

export async function businessPartners(req: Request, res: Response) {
  res.status(200).send(await getAllBusinessPartners());
}