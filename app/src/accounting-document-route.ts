import { Request, Response } from 'express';
import { OperationalAcctgDocItemCube } from '@sap/cloud-sdk-vdm-accounting-document-service';
import { or, and } from '@sap/cloud-sdk-core';

function getAllOperationalAcctgDocItemCube(): Promise<OperationalAcctgDocItemCube[]> {
    return OperationalAcctgDocItemCube.requestBuilder()
        .getAll()
        .select(
            OperationalAcctgDocItemCube.ACCOUNTING_DOCUMENT_CATEGORY_NAME,
            
          )
        //   .filter(or(
        //       and(
        //         BusinessPartner.FIRST_NAME.equals('Jason'),
        //         BusinessPartner.LAST_NAME.equals('Hoskins'),
        //       )
        //   ))
          .top(10)
        //   .skip(0)
        // .withCustomHeaders({
        //     'APIKey': '9OLeNbRsXlZ3U2faS6bD1TnsBCYaU6gn'
        // })
        .execute({
            destinationName: 'my300033'
        });
}

export async function operationalAcctgDocItemCube(req: Request, res: Response) {
  res.status(200).send(await getAllOperationalAcctgDocItemCube());
}