module.exports = function(grunt) {
    grunt.initConfig({
        ts: {
            default : {
                tsconfig: './tsconfig.json',
                src: ["**/*.ts", "!node_modules/**/*.ts"],
                outDir: "dist"
            },
            options: {
            }
        }
    });
    grunt.loadNpmTasks("grunt-ts");
    grunt.registerTask("default", ["ts"]);
   };
